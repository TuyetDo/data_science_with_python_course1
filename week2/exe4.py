#input: List gom N phan tu random trong khoang tu 0-10
#outpur: max, min, mean(trung binh),variance and standard(phuong sai) deviation(Do lech chuan), xoa phan tu trung

import random
def getInput():
    n = int(input('Nhap vao so phan tu cua list: '))
    listNumber = []
    for i in range(n):
        x =  random.randint(0,10)
        listNumber.append(x)
    return listNumber, n

def findMaxMin(listNumber):
    max = listNumber[0]
    min = listNumber[0]
    for i in range(0, len(listNumber)):

        if listNumber[i] > max:
            max = listNumber[i]
        elif listNumber[i] < min:
            min = listNumber[i]

    print('Phan tu lon nhat: {}'.format(max))
    print('Phan tu nho nhat: {}'.format(min))

def findFrequency(listNumber):#Ham tim so lap cua tung phan tu
    listNumberUnduplicated = list(set([i for i in listNumber]))
    print(listNumberUnduplicated)
    frequency = []
    for i in listNumberUnduplicated:
        x = listNumber.count(i)
        frequency.append(x)
    return frequency, listNumberUnduplicated

def action(listNumber, n, frequency, listNumberUnduplicated):
    mean = sum(listNumber)/n
    print('Gia Tri trung binh {}'.format(mean))
    sumFrequency = sum(frequency)
    print('Tong tan so {}'.format(sumFrequency))
    squareListNumber = []
    test = []
    for i in listNumberUnduplicated:
        x = pow(i, 2)
        squareListNumber.append(x)
    for i in range(len(squareListNumber)):
        for j in range(len(frequency)):
            if i == j:
                x = squareListNumber[i]*frequency[j]
                test.append(x)

    print('List binh phuong {}'.format(squareListNumber))
    print('Tong list binh phong {}'.format(sum(test)))
    varianceAndStandard = ((1/sumFrequency)*(sum(test)) - pow(mean, 2))
    deviation = varianceAndStandard**(1/2)

    print('Phuong sai {}'.format(varianceAndStandard))
    print('Do lech chuan {}'.format(deviation))


listNumber, n = getInput()
print(listNumber)
frequency, listNumberUnduplicated = findFrequency(listNumber)
print(frequency)
findMaxMin(listNumber)
action(listNumber, n, frequency, listNumberUnduplicated)