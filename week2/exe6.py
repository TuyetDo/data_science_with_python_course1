# input: Dua vao 1 list gom cac list con( tao thanh ma tran co m: dong, n: cot
# ouput: tinh tom cac column( cot)
def getInput():
    row = int(input('Nhap vao so dong: '))
    column = int(input('Nhap vao so cot: '))
    print('Moi nhap vao {row} list va moi list co {column} phan tu'.format(row = row, column = column))
    print('Nhap theo dinh dang sau:{}'.format('1,2,3,..' ))
    list_input = []

    for i in range(row):

        stringInput = input()
        stringInput = stringInput.split(',')
        list_input.append(stringInput)



    return row, column, list_input

def convertStringToInt(input_list_string): # hàm chuyển từng phần tử trong mảng thành kiểu int:['1','2'] thành [1,2]
    matric = []
    for i in range(len(input_list_string)): #Mỗi một vòng for chạy thì khỏi tạo list_son(list con) mới
        list_son = []
        for j in input_list_string[i]:
            j = int(j)
            list_son.append(j)
        matric.append(list_son)

    return 'Ma tra: {}'.format(matric), matric

def converRowColumnToColumnRow(column, row, matric):#Chuyển ma trận cỡ m dòng, n cột thành ma trận n dòng m cột( ma trận chuyển vị)
    listNew= []

    for i in range(column):
        listOld = []
        for j in range(row):


            listOld.append(matric[j][i])
        listNew.append(listOld)
    return listNew

def sumColumn(listNew):
    listSumColumn = []
    for i in listNew:
        listSumColumn.append(sum(i))

    print('Tong cot la: {}'.format(listSumColumn))




row, column, list_input = getInput()
note, matric = convertStringToInt(list_input)
listMatricNew = converRowColumnToColumnRow(column, row, matric)
sumColumn(listMatricNew)