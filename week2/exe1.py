# input: dua vao 1 list
# Output: In ra list gom so chan(even) va list gom so le(odd)

def even_odd_list():
    input_list = input('Nhap vao mot list cac so tu nhien: ')
    input_list = input_list.split(',')
    even_list, odd_list = [], []
    for i in input_list:
        i = int(i)
        if i % 2 == 0:
            even_list.append(i)
        else:
            odd_list.append(i)

    print('Day so chan: {}'.format(even_list))
    print('Day so le: {}'.format(odd_list))

even_odd_list()
