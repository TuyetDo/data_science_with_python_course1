#input: string s =  'the output that tells you if the object is the type you think or not'
# output: s1 = 'if is not object or output tells that the the the think type you you'
#         s2 = 'The Output That Tells You If The Object Is The Type You Think Or Not'

from operator import itemgetter
s = 'the output that tells you if the object is the type you think or not'
def takeFirst(elem):
    return elem[0], elem[1]

def outputS1():
    list_s = s.split(' ')
    list_s.sort(key = takeFirst)
    s1 = ''
    for i in list_s:
        s1 += i
        s1 = s1 + ' '


    print(s1)
def outputS2():
    s2 = ''
    test = ' '
    list_s = s.split(' ')

    for i in list_s:
        s2 += i.capitalize()
        s2 = s2+ ' '


    print(s2)

outputS1()
outputS2()
